<?php

/**
 * Implements hook_uninstall().
 */
function blog_uninstall() {
  Drupal::configFactory()->getEditable('core.entity_form_display.node.blog_post.default')->delete();
  Drupal::configFactory()->getEditable('core.entity_view_display.node.blog_post.default')->delete();
  Drupal::configFactory()->getEditable('core.entity_view_display.node.blog_post.teaser')->delete();
  Drupal::configFactory()->getEditable('field.field.node.blog_post.body')->delete();
  Drupal::configFactory()->getEditable('field.field.node.blog_post.field_blog_comments')->delete();
  Drupal::configFactory()->getEditable('field.field.node.blog_post.field_blog_tags')->delete();
  Drupal::configFactory()->getEditable('field.storage.node.field_blog_comments')->delete();
  Drupal::configFactory()->getEditable('field.storage.node.field_blog_tags')->delete();
  Drupal::configFactory()->getEditable('node.type.blog_post')->delete();
  Drupal::configFactory()->getEditable('views.view.blog')->delete();
}
